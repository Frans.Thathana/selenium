package application;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import dataFunctions.dataFuntions;
import utilities.utilities;

public class application {
	dataFuntions dataFuntions=new dataFuntions();
	
	public void applyOnline(WebDriver driver,Properties prop) throws InterruptedException
	{
		
		  driver.findElement(By.xpath(prop.getProperty("careers"))).click();
		  
		  driver.findElement(By.xpath(prop.getProperty("southAfrica"))).click();
		  
		  driver.findElement(By.xpath(prop.getProperty("opening"))).click();
		  
		  driver.findElement(By.xpath(prop.getProperty("applyOnline"))).click();
		  
	}
	public void apply(int i,Sheet s1,WebDriver driver,Properties prop) throws Exception {
		
		driver.findElement(By.xpath(prop.getProperty("entername"))).sendKeys(dataFuntions.getCellData("name",i,s1));
		driver.findElement(By.xpath(prop.getProperty("enteremail"))).sendKeys(dataFuntions.getCellData("email",i,s1));
		driver.findElement(By.xpath(prop.getProperty("entercontact"))).sendKeys(dataFuntions.getCellData("phone",i,s1));
		driver.findElement(By.xpath(prop.getProperty("message"))).sendKeys(dataFuntions.getCellData("work",i,s1));
		driver.findElement(By.xpath(prop.getProperty("sendApplication"))).click();
			
		
	
		
	
	}
	public void clearField(int i,Sheet s1,WebDriver driver,Properties prop) {
		driver.findElement(By.xpath(prop.getProperty("entername"))).clear();
		driver.findElement(By.xpath(prop.getProperty("enteremail"))).clear();
		driver.findElement(By.xpath(prop.getProperty("entercontact"))).clear();
		driver.findElement(By.xpath(prop.getProperty("message"))).clear();
	}

}
