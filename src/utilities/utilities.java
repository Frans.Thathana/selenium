package utilities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;

import dataFunctions.dataFuntions;
import test.NewTest;

public class utilities {
	
	public WebDriver driver=null;
	public ExtentReports extent=null;

	public WebDriver navigateUrl() throws IOException {
		 System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		 driver=new ChromeDriver();
		 dataFuntions dt=new dataFuntions();
		 Properties prop = dt.loadProperty();
		 driver.manage().window().maximize();
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		return driver;
	}
	
	public void colapse(ExtentReports extent,WebDriver driver) {
		
		extent.flush();
		  driver.quit();
	}
	
	
	
}
