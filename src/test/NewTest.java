package test;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.codoid.products.fillo.Select;

import application.application;
import dataFunctions.dataFuntions;
import reporting.reporting;
import schemasMicrosoftComVml.STExt;
import utilities.utilities;

public class NewTest {
	utilities utilities = new utilities();
	application application= new application();
	dataFuntions dataFuntions=new dataFuntions();
	reporting reporting = new reporting();
	WebDriver driver=null;
	ExtentReports extent=null;
	Properties prop=null;
  @BeforeMethod
  public void b() throws IOException {
	driver=utilities.navigateUrl();
	prop=dataFuntions.loadProperty();
	}
  @Test
  public void t() throws Exception {
	  XSSFWorkbook wk = new XSSFWorkbook(".\\data\\Book1.xlsx");
		XSSFSheet s1 = wk.getSheet("Sheet1");
	  int rowCount=s1.getPhysicalNumberOfRows();
	 
	   application.applyOnline(driver,prop);
	  
	  for(int i=1;i<rowCount;i++) {
			application.apply(i, s1, driver,prop);
			
			
			extent =reporting.frans(driver,prop);
			application.clearField(i, s1, driver, prop);
			
			}
		
}
@AfterMethod
public void a() {
	
	   utilities.colapse(extent,driver);
	  }
}
		 			
		