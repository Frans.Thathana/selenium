package reporting;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class reporting {
	public String takeScreenShot( WebDriver driver) throws IOException {
		  String fileName="empty";
		  File srcfile=((TakesScreenshot) (driver)).getScreenshotAs(OutputType.FILE);
		  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		  fileName=".\\screen\\"+fileName+timeStamp+".png";
		  
		  FileUtils.copyFile(srcfile, new File(fileName));
		  return fileName;
		  
	  }
	 public ExtentTest creatTest(ExtentReports extent) {
		  
			ExtentTest test= extent.createTest("ILAB POI TEST").pass("details");
			  
			return test;
			  
		  }
	 
	 public ExtentReports extendReport(String reports) {
		  ExtentHtmlReporter  HtmlReporter=new ExtentHtmlReporter(reports);
		  ExtentReports extent = new ExtentReports();
		
		 
		 HtmlReporter.config().setTheme(Theme.DARK);
		 extent.attachReporter(HtmlReporter);
		  return extent;
		  
		
	  }
	 public ExtentReports frans(WebDriver driver,Properties prop) throws IOException {
		 
		 String stext=driver.findElement(By.xpath(prop.getProperty("err"))).getText();
		 ExtentReports extent = extendReport(".\\reports\\rep.html");
			  ExtentTest test=  creatTest(extent);
			  String filename =  takeScreenShot( driver);
		
			  if(stext.equals("There are errors in your form.")) {
				  test.log(Status.PASS, stext);
				  test.pass("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath("."+filename).build());
			
			  }else {
				  test.log(Status.FAIL, stext);
				  test.fail("fail");
				  }
			  return extent;
	 }
	 
	 

}
